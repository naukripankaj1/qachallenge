﻿using MySql.Data.MySqlClient;
using System;

namespace LDMSTest
{
    public class DBConnection
    {
        private string _serverName;
        private string _database;
        private string _userId;
        private string _password;
        private MySqlConnection _connection;
        private MySqlCommand cmd;
        string query = null;

        public DBConnection()
        {
            _serverName = "sql2.freemysqlhosting.net";
            _database = "sql2394914";
            _userId = "sql2394914";
            _password = "nV2%iI7%";
            string connectionString = "SERVER=" + _serverName + ";" + "DATABASE=" + _database + ";" + "UID=" + _userId + ";" + "PASSWORD=" + _password + ";";

            _connection = new MySqlConnection(connectionString);
        }

        private bool OpenConnection()
        {
            try
            {
                _connection.Open();
                return true;
            }
            catch (MySqlException)
            {
                return false;
            }
        }

        private bool CloseConnection()
        {
            try
            {
                _connection.Close();
                return true;
            }
            catch (MySqlException)
            {
                return false;
            }
        }

        public void InsertDataIntoDepartmentTable()
        {
            query = "INSERT INTO Department(DepartmentId, DepartmentName, Location) VALUES" +
                "('1', 'Management', 'London'), ('2', 'Engineering', 'Cardiff'), ('3', 'Research', 'Edinburgh'), ('4', 'Sales', 'Bristol')";

            if (OpenConnection())
            {
                cmd = new MySqlCommand(query, _connection);
                cmd.ExecuteNonQuery();
                CloseConnection();
            }
            else
                Console.WriteLine("There's issue connecting with DB.");
        }

        public void InsertDataIntoEmployeeTable(int requiredRows)
        {
            if (OpenConnection())
            {
                for (int i = 1; i <= requiredRows; i++)
                {
                    query = "INSERT INTO Employee(EmployeeID, EmployeeName, JobTitle, ManagerID, DateHired, Salary, DepartmentId) VALUES" +
                            "('" + i + "', 'Name" + i.ToString() + "', 'JobTitle " + i.ToString() + "', '" + (i - 1) + "', '" + Utility.RandomDay() + "', '" + Utility.GetRandomNumer(10000, 25001) + "', '" + Utility.GetRandomNumer(1, 5) + "')";
                    cmd = new MySqlCommand(query, _connection);
                    cmd.ExecuteNonQuery();
                }
                CloseConnection();
            }
            else
                Console.WriteLine("There's issue connecting with DB.");
        }

        public bool CheckIfTableHasData(string tableName)
        {
            if (OpenConnection())
            {
                query = "select * from " + tableName;
                using (cmd = new MySqlCommand(query, _connection))
                {
                    MySqlDataReader reader = cmd.ExecuteReader();
                    bool hasRows = reader.HasRows;
                    CloseConnection();
                    return hasRows;
                }
            }
            else
            {
                Console.WriteLine("There's issue connecting with DB.");
                return false;
            }
        }

        public void DeleteDataFromTable(string tableName)
        {
            if (OpenConnection())
            {
                query = "DELETE FROM " + tableName;
                cmd = new MySqlCommand(query, _connection);
                cmd.ExecuteNonQuery();
                CloseConnection();
            }
            else
                Console.WriteLine("There's issue connecting with DB.");
        }

        //Report-Employees in Department for a specified Department Id
        public void PrintReportOne(string deptID)
        {
            if (OpenConnection())
            {
                query = "SELECT EmployeeID, EmployeeName, JobTitle, Salary FROM Employee WHERE DepartmentID = " + deptID;
                cmd = new MySqlCommand(query, _connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();
                if (dataReader.HasRows == true)
                {
                    Console.WriteLine("EployeeID          Name          job_Title           Salary");
                    while (dataReader.Read())
                    {
                        var empId = dataReader["EmployeeID"];
                        var empName = dataReader["EmployeeName"];
                        var job = dataReader["JobTitle"];
                        var salary = dataReader["Salary"];
                        Console.WriteLine(empId + "             " + empName + "             " + job + "             " + salary);
                    }
                }
                else
                {
                    Console.WriteLine("There's no recods in the database for DepartmentID : " + deptID);
                }
                dataReader.Close();
                CloseConnection();
            }
            else
                Console.WriteLine("There's issue connecting with DB.");
        }

        //Report-Employees in Location for a specified Location
        public void PrintReportTwo(string location)
        {
            if (OpenConnection())
            {
                query = "SELECT t1.EmployeeID, t1.EmployeeName, t1.JobTitle, t1.Salary, t2.DepartmentName FROM Employee t1 INNER JOIN Department t2 ON t1.DepartmentID = t2.DepartmentId WHERE (t2.Location = '" + location + "')";
                cmd = new MySqlCommand(query, _connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();
                if (dataReader.HasRows == true)
                {
                    Console.WriteLine("EployeeID      Name          job_Title           Salary      DepartmentName");
                    while (dataReader.Read())
                    {
                        var empId = dataReader["EmployeeID"];
                        var empName = dataReader["EmployeeName"];
                        var job = dataReader["JobTitle"];
                        var salary = dataReader["Salary"];
                        var dept = dataReader["DepartmentName"];
                        Console.WriteLine(empId + "         " + empName + "         " + job + "         " + salary + "      " + dept);
                    }
                }
                else
                {
                    Console.WriteLine("There's no recods in the database for location : " + location);
                }
                dataReader.Close();
                CloseConnection();
            }
            else
                Console.WriteLine("There's issue connecting with DB.");
        }
    }
}
