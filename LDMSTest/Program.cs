﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace LDMSTest
{
    public class Program
    {
        static void Main(string[] args)
        {
            DBConnection conn = new DBConnection();
            //Check if department table has no data -> create data
            if (!conn.CheckIfTableHasData("Department"))
                conn.InsertDataIntoDepartmentTable();

            //check if employee table has data -> delete older data 
            if (conn.CheckIfTableHasData("Employee"))
                conn.DeleteDataFromTable("Employee");

            //Insert Data into employee table
            Console.WriteLine("Please enter a number for required data rows in Employee table: ");
            string rows = Console.ReadLine();
            conn.InsertDataIntoEmployeeTable(Convert.ToInt32(rows));

            //Print the first report->Employees in Department for a specified Department Id
            Console.WriteLine("Please enter a department ID(1-4) for report: Employees in Department");
            string dept = Console.ReadLine();
            conn.PrintReportOne(dept);

            //Print the second report->Employees in Location for a specified Location
            Console.WriteLine("Please enter a Location(London, Cardiff, Edinburgh, Bristol) for report: Employees in Location");
            string location = Console.ReadLine();
            conn.PrintReportTwo(location);
            Console.ReadLine();


            //Tests
            
            /*
             Test 1:   Report 1- Validate that the number of rows in report 1 should be equal to the result of query -
             
              "SELECT COUNT(*) AS Expr1
                FROM Employee
                WHERE (DepartmentID = <DepartmentID>)"
            
             
            Test 2:   Report 2- Validate that the number of rows in report 2 should be equal to the result of query -
             
              "SELECT COUNT(*) FROM Employee t1 
                INNER JOIN Department t2 
                ON t1.DepartmentID = t2.DepartmentId
                WHERE  (t2.Location = '<city name>')"
            
             */

        }
    }
}
