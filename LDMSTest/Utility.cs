﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LDMSTest
{
    public class Utility
    {
        public static int GetRandomNumer(int startIndex, int endIndex)
        {
            Random ra = new Random();
            return ra.Next(startIndex, endIndex);
        }

        public static string RandomDay()
        {
            Random ra = new Random();
            DateTime start = new DateTime(1995, 1, 1);
            int range = (DateTime.Today - start).Days;
            return start.AddDays(ra.Next(range)).ToString("dd-MM-yy");
        }
    }
}
